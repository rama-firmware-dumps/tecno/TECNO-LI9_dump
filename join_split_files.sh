#!/bin/bash

cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/Pova6ProWallpaper/Pova6ProWallpaper.apk.* 2>/dev/null >> product/app/Pova6ProWallpaper/Pova6ProWallpaper.apk
rm -f product/app/Pova6ProWallpaper/Pova6ProWallpaper.apk.* 2>/dev/null
cat system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null >> system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk
rm -f system_ext/priv-app/TranSettingsApk/TranSettingsApk.apk.* 2>/dev/null
cat system_ext/priv-app/TranAodApk/TranAodApk.apk.* 2>/dev/null >> system_ext/priv-app/TranAodApk/TranAodApk.apk
rm -f system_ext/priv-app/TranAodApk/TranAodApk.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system/system/apex/com.android.btservices.apex.* 2>/dev/null >> system/system/apex/com.android.btservices.apex
rm -f system/system/apex/com.android.btservices.apex.* 2>/dev/null
